//Jeremy Okeyo jokeyo2
//Darren Yang dyang44
#include "ppm_io.h"
#define PI 3.14
#define sq(r) r * r

#ifndef IMAGEMANIP
#define IMAGEMANIP


unsigned char saturation(unsigned char data, int value);
//Brightens the image, returning a copy of the manipulated image
Image * brighten(Image *image, int value);

//Crops the image, returning a copy of the manipulated image
Image * crop(Image *image, int topCol, int topRow, int botCol, int botRow);
double ** gaus_filter(double sigma, int N);
void convolve(Image *image, int pxl_row, int pxl_col, Pixel *pxl, double **matrix, int N);

//Blurs the image and returns a copy of the manipulated image
Image * blur(Image *image, double sigma);

// Converts an image to grayscale and returns a copy of the manipulated image
Image * grayscale(Image * image);

// Blacks out an area of the given image given the top left and bottom right corner pixel locationsa nd returns a copy of the manipulated image
Image * occlude(Image * image, int top_left_x, int top_left_y, int bottom_right_x, int bottom_right_y);

// Performs an edge detection on the given image and returns a copy of the manipulated image
Image * edge(Image * image, double sigma, double threshold);

#endif
