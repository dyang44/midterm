// Jeremy Okeyo
// jokeyo2
// Darren Yang
// dyang44
#include "imageManip.h"
#include "ppm_io.h"
#include <assert.h>

void test_ppm() {
  FILE *file = fopen("nika.ppm", "r");
  Image *im = read_ppm(file);
  FILE *out = fopen ("tst_out_test.ppm","w");
  int pixels = write_ppm(out, im);

  delete_ppm(im);

  //assert(pixels != (im->rows * im->cols));
   
  fclose(file);
  fclose(out);
}

void test_create() {
  FILE *out = fopen("tst_out.ppm", "w");
  Image *img_cpy = create_ppm(500, 500);
  int pixels = write_ppm(out, img_cpy);

  delete_ppm(img_cpy);
  fclose(out);
}

void test_brightness() {
  FILE *file = fopen("nika.ppm", "r");
  Image *im = read_ppm(file);
  FILE *out = fopen ("tst_brightness1.ppm","w");
  FILE *out2 = fopen ("tst_brightness2.ppm","w");
  FILE *out3 = fopen ("tst_brightness3.ppm","w");


  Image *im_cpy = brighten(im, 75);
  int pixels = write_ppm(out, im_cpy);
  Image *im_cpy_2 = brighten(im, -25);
  pixels = write_ppm(out2, im_cpy_2);
  Image *im_cpy_3 = brighten(im, -75);
  pixels = write_ppm(out3, im_cpy_3);

  delete_ppm(im);
  delete_ppm(im_cpy);
  delete_ppm(im_cpy_2);
  delete_ppm(im_cpy_3);

  fclose(file);
  fclose(out);
  fclose(out2);
  fclose(out3);
}

void test_crop() {
  FILE *file = fopen("nika.ppm", "r");
  Image *im = read_ppm(file);
  FILE *out = fopen ("tst_crop.ppm","w");

  Image *im_crp = crop(im, 60, 15, 600, 565);
  int pixels = write_ppm(out, im_crp);

  delete_ppm(im);
  delete_ppm(im_crp);
  fclose(file);
  fclose(out);
}

void test_blur() {
  FILE *file = fopen("nika.ppm", "r");
  Image *im = read_ppm(file);
  FILE *out = fopen ("tst_blur.ppm","w");

  Image *im_blur = blur(im, 2);
  int pixels = write_ppm(out, im_blur);

  delete_ppm(im);
  delete_ppm(im_blur);
  fclose(file);
  fclose(out);

}

void test_grayscale() {
  FILE * file = fopen("nika.ppm", "r");
  Image * img = read_ppm(file);
  FILE * output = fopen("tst_grayscale.ppm", "w");

  Image * img_grayscale = grayscale(img);
  int pixels = write_ppm(output, img_grayscale);

  delete_ppm(img);
  delete_ppm(img_grayscale);
  fclose(file);
  fclose(output);
}

void test_occlude() {
  FILE * file = fopen("nika.ppm", "r");
  Image * img = read_ppm(file);
  FILE * output = fopen("tst_occlude.ppm", "w");

  Image * img_occlude = occlude(img, 250, 280, 530, 370);
  int pixels = write_ppm(output, img_occlude);

  delete_ppm(img);
  delete_ppm(img_occlude);
  fclose(file);
  fclose(output);
}

void test_edges() {
  FILE * file = fopen("nika.ppm", "r");
  Image * img = read_ppm(file);
  FILE * output = fopen("tst_edges.ppm", "w");

  Image * img_edges = edge(img, 0.5, 8);
  int pixels = write_ppm(output, img_edges);
  delete_ppm(img);
  delete_ppm(img_edges);
  fclose(file);
  fclose(output);
}
int main() {
  test_ppm();
  test_create();
  test_brightness();
  test_crop();
  test_blur();
  test_grayscale();
  test_occlude();
  test_edges();
  return 0;
}
