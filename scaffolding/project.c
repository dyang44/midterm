// Darren Yang
// dyang44
// Main program that processes a given image based on the user's input.

#include "imageManip.h"
#include "ppm_io.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int is_number(char str[]);

// Parse user input and perform the corresponding operation.
int process_image(int argc, char* argv[]) {
  if(argc <= 1)
    return 1;
  char *input_filename = argv[1];
  char *input_ending = strrchr(input_filename, '.');

  if(strcmp(input_ending, ".ppm") != 0)
    return 1; // Failed to supply input fiename

  FILE *input = fopen(input_filename, "r");
  if(input == NULL)
    return 2; // Specified input file could not be opened

  Image *img = read_ppm(input);
  if(img == NULL)
    return 3; // Specified input file is not a properly-formatted PPM file

  fclose(input);
  
  char *operation = argv[3];
  Image *edited_img = create_ppm(img->rows, img->cols);
  for(int i = 0; i < (int)strlen(operation); i++) {
    operation[i] = tolower(operation[i]);
  }
  delete_ppm(edited_img);

  if(strcmp(operation, "bright") == 0) {
    if(argc != 5 || !is_number(argv[4]))
      return 5; // Incorrect number of arguments
    int param = atoi(argv[4]);
    edited_img = brighten(img, param);
  }
  else if(strcmp(operation, "gray") == 0) {
    edited_img = grayscale(img);
  }
  else if(strcmp(operation, "crop") == 0) {
    if(argc != 8 || !is_number(argv[4]) || !is_number(argv[5]) || !is_number(argv[6]) || !is_number(argv[7]))
      return 5; // Incorrect number of arguments
    int param_x1 = atoi(argv[4]);
    int param_y1 = atoi(argv[5]);
    int param_x2 = atoi(argv[6]);
    int param_y2 = atoi(argv[7]);
    if(param_x2 < param_x1 || param_y2 < param_y1 || param_x1 > img->cols || param_x2 > img->rows || param_y1 > img->rows || param_y2 > img->rows)
      return 6; // Senseless input
    edited_img = crop(img, param_x1, param_y1, param_x2, param_y2);
  }
  else if(strcmp(operation, "occlude") == 0) {
    if(argc != 8 || !is_number(argv[4]) || !is_number(argv[5]) || !is_number(argv[6]) || !is_number(argv[7]))
      return 5; // Incorrect number of arguments
    int param_x1 = atoi(argv[4]);
    int param_y1 = atoi(argv[5]);
    int param_x2 = atoi(argv[6]);
    int param_y2 = atoi(argv[7]);
    if(param_x2 < param_x1 || param_y2 < param_y1 || param_x1 > img->cols || param_x2 > img->rows || param_y1 > img->rows || param_y2 > img->rows)
      return 6; // Senseless input
    edited_img = occlude(img, param_x1, param_y1, param_x2, param_y2);
  }
  else if(strcmp(operation, "blur") == 0) {
    if(argc != 5 || !is_number(argv[4]))
      return 5; // Incorrect number of arguments
    double sigma = atof(argv[4]);
    edited_img = blur(img, sigma);// TODO: Blur output file not created
  }
  else if(strcmp(operation, "edges") == 0) {
    if(argc != 6 || !is_number(argv[4]) || !is_number(argv[5]))
      return 5; // Incorrect number of arguments
    double sigma = atof(argv[4]);
    int threshold = atoi(argv[5]);
    edited_img = edge(img, sigma, threshold);
  }
  else {
    return 4;
  }
  
  char *output_filename = argv[2];
  char *output_ending = strrchr(output_filename, '.');
  
  if(strcmp(output_ending, ".ppm") != 0)
    return 1; // Failed to supply output filename

  FILE * output = fopen(output_filename, "w");
  if(output == NULL)
    return 7; // Specified output file could not be opened for writing

  write_ppm(output, edited_img);

  delete_ppm(img);
  delete_ppm(edited_img);
  fclose(output);

  return 0;
}

int main(int argc, char *argv[]) {
  int value = 0;
  value = process_image(argc, argv);
  return value;
}

int is_number(char str[]) {
  int check = 0;

  for(int i = 0; i < (int)strlen(str); i++) {
    if(isdigit(str[i]) == 0 || str[i] != 46 || str[i] != 45)
       check = 1;
  }

  return check;
}
