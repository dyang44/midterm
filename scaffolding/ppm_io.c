// Jeremy Okeyo Jokeyo2
// Darren Yang dyang44
// 
// ppm_io.c
// 601.220, Spring 2019
// Starter code for midterm project - feel free to edit/add to this file

#include <assert.h>
#include "ppm_io.h"
#include <stdlib.h>
#include <string.h>


/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

  assert(fp); // Check that file pointer is not NULL 
  int row = 0, col = 0, color = 255, details = 1;
  const char format[2] = {'P','6'};
  
  //Checks whether it is the proper file format
  while (details < 4) {
  	char input[15];
	fgets(input, 70, fp);
	if (input[0] == '#') {
		continue;	
    }
	switch (details) {
	  case 1:
		if (strcmp(input, format)) {
			details++;
		}
		else {
			return NULL;
		}
		break;
	  case 2:
 		sscanf(input, "%d %d", &col, &row);
		details++;
		break;
	  case 3:
		sscanf(input, "%d", &color);
		if (color != 255){
			return NULL;
		}
		details++;
		break;
	}
  }
  
  //Pixel *pxl =(Pixel*) calloc((row * col),sizeof(Pixel)); 
  Pixel *pxl = malloc(row*col*sizeof(Pixel));
  //Assigns pixels RGB values
  int count = 0;
  while (count < (row * col)) {
	char pntr[3];
   	fread(pntr, 1, 3, fp);
	pxl[count].r = pntr[0];
	pxl[count].b = pntr[2];
	pxl[count].g = pntr[1];
	count++;
  }
 
  //Assigns image Pixel and size
  Image *image = malloc(sizeof(Image));
  image->data = pxl;
  image->rows = row;
  image->cols = col;
    
  return image;
}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp); 

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}

/** Copies an image based on a given image and returns the Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * copy_ppm(const Image *im) {
  
  Pixel * pcopy = (Pixel *)calloc((im->rows * im->cols), sizeof(Pixel));
  
  // Copies RGB values
  int count = 0;
  while (count < (im->rows*im->cols)) {
    pcopy[count].r = im->data[count].r;
    pcopy[count].g = im->data[count].g;
    pcopy[count].b = im->data[count].b;
    count++;
  }
 
  //Assigns image Pixel and size
  Image * copy = malloc(20);
  copy->data = pcopy;
  copy->rows = im->rows;
  copy->cols = im->cols;
  
  return copy;
}

/** Creates a blank image given a specified number of rows and columns */
Image * create_ppm(int rows, int cols) {
  Pixel * pcopy = (Pixel *)calloc((rows*cols), sizeof(Pixel));

  for(int i = 0; i < rows*cols; i++) {
    pcopy[i].r = 255;
    pcopy[i].g = 255;
    pcopy[i].b = 255;
  }

  Image * copy = malloc(sizeof(Image));
  copy->data = pcopy;
  copy->rows = rows;
  copy->cols = cols;

  return copy;
}

/** Frees up the memory location of a given address of an Image.
 */
int delete_ppm(Image *im) {
  free(im->data);
  free(im);
  return 0;
}
