//Jeremy Okeyo jokeyo2
//Darren Yang dyang44

#include "imageManip.h"
#include "ppm_io.h"
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
 

/** 
 * Helper method used to saturate individual pixel color using value provided.
 * Returns the saturation value as an unsigned char
 * TODO could include modes for multiple uses
 */
unsigned char saturation(unsigned char data, int value) {
	double saturate = (double)data + value;
	if (saturate < 0) {
		saturate = 0;
	}
	else if (saturate > 255) {
		saturate = 255;
	}
	return (unsigned char)saturate;
}

/** 
 * Brightens (or darkens) ppm image provided depending on the value passed.
 * Positive values, brighten. Negative values, darken.
 * Returns a manipulated copy of the original image.
 */
Image * brighten(Image *image, int value) {
	int num_pixels = image->cols * image->rows;
	Image *image_cpy = copy_ppm(image);
	for (int i = 0; i < num_pixels; i++) {
		image_cpy->data[i].r = saturation(image->data[i].r, value);
		image_cpy->data[i].b = saturation(image->data[i].b, value);
		image_cpy->data[i].g = saturation(image->data[i].g, value);
	}
	return image_cpy;
}	

Image * crop(Image *image, int top_col, int top_row, int bot_col, int bot_row) {
	int row = bot_row - top_row;
	int col = bot_col - top_col;
	int crop_pxl = row * col;
	Pixel *pxl = calloc(crop_pxl, sizeof(Pixel));

	//Writes cropped pixeled in new pixel array
 	int written = 0;
	for (int row_count = top_row - 1; row_count < bot_row - 1; row_count++) {
		for (int col_count = top_col - 1; col_count < bot_col - 1; col_count++) {
			int index = ((row_count - 1) * image->cols) + col_count;
			pxl[written] = image->data[index];
			written++;
		}
	}

	Image *image_cpy = malloc(sizeof(Image));
	image_cpy->data = pxl;
	image_cpy->rows = row;
	image_cpy->cols = col;
	return image_cpy;
}

double ** gaus_filter(double sigma, int N) {
  double** matrix = malloc(N*sizeof(double*));
  for(int i = 0; i < N; i++) {
    matrix[i] = malloc(N*sizeof(double));
  }
	for (int i = 0; i < N; i++) {
	  double dx = fabs((double)((int)((N - 1) / 2) - i));
       	  for (int j = 0; j < N; j++) {
	    double dy = fabs((double)((int)((N - 1) / 2) - j));
	    matrix[i][j] = (1.0 / (2.0 * PI * sq(sigma))) * exp(-(sq(dx) + sq(dy)) / (2 * sq(sigma)));
	  }
	}
	
  return matrix;
}

void convolve(Image *image, int pxl_row, int pxl_col, Pixel *pxl,     
	double **matrix, int N) {

  //Image * image_cpy = copy_ppm(image);
  double sum_matrix = 0;
  //, sum_weight = 0;
   	//int color = (int)value, result = 0;
	double sum_weighr = 0, sum_weighb = 0, sum_weighg = 0;
	//Top left pixel from image overlayed with Gaussian matrix
	int row = pxl_row - (N / 2);
	int col = pxl_col - (N / 2);
	for (int i = 0; i < N;i++) {
	   for (int j = 0; j < N;j++) {
		    if ((row + i) < 0 || (row + i) >= image->rows || (col + j) < 0 || (col + j) >= image->cols) {
				continue;
			}
		    int index = ((row + i) * image->cols) + (col + j);
			
			sum_matrix += matrix[i][j];
			sum_weighr += (matrix[i][j] * (int)image->data[index].r);
			sum_weighb += (matrix[i][j] * (int)image->data[index].b);
			sum_weighg += (matrix[i][j] * (int)image->data[index].g);
	   }
	   col = pxl_col - (N / 2);
	}
	
	//delete_ppm(image_cpy);
       
	int weighr = sum_weighr / sum_matrix;
	int weighb = sum_weighb / sum_matrix;
	int weighg = sum_weighg / sum_matrix;
 
	pxl->r = weighr;
	pxl->b = weighb;
	pxl->g = weighg;
}

Image * blur(Image *image, double sigma) {
  int N = 10 * sigma;
	if (!(N % 2)) {
		N += 1;
	}
	double **Gaus_matrix = gaus_filter(sigma, N);
	Image *image_cpy = copy_ppm(image);
	for (int i = 0; i < image->rows; i++ ) {
		for (int j = 0; j < image->cols; j++) {	
			int index = (i * image->cols) + j;
			 convolve(image, i, j, &image_cpy->data[index], Gaus_matrix, N); 			
			 //convolve(image, i, j, &image_cpy->data[index], Gaus_matrix, N); 
			 //convolve(image, i, j, &image_cpy->data[index], Gaus_matrix, N); 	
		}
	}

	for (int i = 0; i < N; i++) {
		free(Gaus_matrix[i]);
	}
	free(Gaus_matrix);
	return image_cpy;
}	

Image * grayscale(Image * image) {
  int num_pixels = image->rows * image-> cols;
  Image * img_copy = copy_ppm(image);
  
  for(int i = 0; i < num_pixels; i++) {
    double intensity = 0.30 * image->data[i].r + 0.59 * image->data[i].g + 0.11 * image->data[i].b;
    unsigned char char_intensity = (unsigned char)intensity;

    img_copy->data[i].r = char_intensity;
    img_copy->data[i].g = char_intensity;
    img_copy->data[i].b = char_intensity;
  }

  return img_copy;
}

Image * occlude(Image * image, int top_left_x, int top_left_y, int bottom_right_x, int bottom_right_y) {
  int start_pixel = top_left_x + image->cols * top_left_y;
  int end_pixel = bottom_right_x + image->cols * bottom_right_y;
  int length = bottom_right_x - top_left_x;
  // int width = bottom_right_y - top_left_y;
  Image * img_copy = copy_ppm(image);
  
  int working_pixel = start_pixel;

  while(working_pixel < end_pixel) {
    for(int i = working_pixel; i < working_pixel+length; i++) {
      img_copy->data[i].r = 0;
      img_copy->data[i].g = 0;
      img_copy->data[i].b = 0;
    }
    working_pixel += img_copy->cols;
  }

  return img_copy;
}

Image * edge(Image *image, double sigma, double threshold) {
  //Image * working_image = grayscale(image);
  Image * edited_image = blur(image, sigma);
  Image * working_image = grayscale(edited_image);
  delete_ppm(edited_image);
  //Pixel two_d_img[working_image->rows][working_image->cols];
  //Pixel * two_d_img = (Pixel *)calloc(working_image->rows*working_image->cols, sizeof(Pixel));
  Pixel** two_d_img = malloc(working_image->rows*sizeof(Pixel*));
  for(int i = 0; i < working_image->rows; i++) {
    two_d_img[i] = malloc(working_image->cols*sizeof(Pixel));
  }
  int row = 0;
  int col = 0;
  
  for(int i = 0; i < working_image->rows*working_image->cols; i++) {
    two_d_img[row][col] = working_image->data[i];
    col++;
    if(col == working_image->cols) {
      row++;
      col = 0;
    }
  }

  // printf("%d %d\n", working_image->cols-1, working_image->rows-1);
  
  for(int i = 1; i < working_image->rows-1; i++) {
    for(int j = 1; j < working_image->cols-1; j++) {
      double grad_x = (two_d_img[i+1][j].r - two_d_img[i-1][j].r)/2;
      double grad_y = (two_d_img[i][j+1].r - two_d_img[i][j-1].r)/2;
      double magnitude_grad = sqrt(pow(grad_x, 2) + pow(grad_y, 2));

      // printf("%d %d %d \n", grad_x, grad_y, magnitude_grad);
      
      int pixel_number = i * working_image->cols + j;
      //printf("%d\n", pixel_number);
      
      if(magnitude_grad > threshold) {
	working_image->data[pixel_number].r = 0;
	working_image->data[pixel_number].g = 0;
	working_image->data[pixel_number].b = 0;
      }
      else {
	working_image->data[pixel_number].r = 255;
	working_image->data[pixel_number].g = 255;
	working_image->data[pixel_number].b = 255;
      }
    }
  }

  for(int i = 0; i < working_image->rows; i++) {
    free(two_d_img[i]);
  }
  free(two_d_img);
  return working_image;
}
